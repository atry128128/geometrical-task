import React from 'react';
import Result from './Result';
import FormSearch from './FormSearch';
import {Provider} from "react-redux";
import store from "../redux/store";
import styled from "styled-components";

const AppContainer = styled.div`
    padding: 1rem;
    height: 100vh;
`;

const AppBox = styled.div`
    width: 100%;
    height: 100%;
    flex: 1 1;
`;

function App() {
    return (
            <Provider store={store}>
                <AppContainer>
                    <AppBox>
                        <FormSearch/>
                        <Result/>
                    </AppBox>
                </AppContainer>
            </Provider>
    );
}

export default App;

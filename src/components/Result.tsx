import React, {useState} from "react";
import RectanglesList from './RectanglesList';
import {RequestRectanglesType} from "../types/RequestType";
import {getIdValue} from '../redux/selectors';
import {RootState} from "../types/StoreTypes";
import {connect} from "react-redux";
import {setIdValue as setIdValueRedux} from "../redux/actions";

type Item = {
    rotation?: number;
}

type PropsTypeIdValue = {
    idValue: string;
}

const Result = (props: PropsTypeIdValue) => {

    const id = props.idValue;
    const URL_API = `https://recruitment01.vercel.app/api/project/${id}`;
    const [dataRectangles, setDataRectangles] = useState<RequestRectanglesType>();
    const [error, setError] = useState<string>();

    React.useEffect(() => {
        if (id) {
            fetch(URL_API)
                .then(response => response.json())
                .then((data: RequestRectanglesType) => {
                    setDataRectangles(data);
                })
                .then(() => {
                    setError(undefined);
                    checkValidDataFromAPI();
                })
                .catch((fetchError) => {
                    setError(`🦊 There has been a problem with your fetch operation: ${fetchError}`)
                });
        }
    }, [props.idValue, dataRectangles?.project.name]);

    function checkValidDataFromAPI() {
        dataRectangles ? dataRectangles?.project?.items?.map((item: Item) => {
            if (isNaN(item.rotation as number)) {
                setError("🦔 invalid data!")
                return error;
            }
        }) : setError(" 🦔 invalid data!");

        // todo- dodać resztę warunków, np. szerokość elementu nie może być mniejsza/równa od zera, itp.
    }

    return (
        <>
            {error ? <p>{error}</p>
                :
                <div>
                    {id && <div>name: <b>{dataRectangles?.project?.name}</b></div>}
                    {id && <div>ID: <b>{dataRectangles?.id} </b></div>}
                    <RectanglesList data={dataRectangles}/>
                </div>
            }
        </>
    )
}
const mapPropsToState = (state: RootState) => ({
    idValue: getIdValue(state)

});
export default connect(mapPropsToState, {setIdValueRedux})(Result);
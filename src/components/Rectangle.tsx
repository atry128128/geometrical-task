import React from "react";
import {RectangleType as _RectangleType} from "../types/RectangleType";

type RectangleType = {
    rectangleItem: _RectangleType;
};

type DimensionsBorder = {
    width: number,
    height: number,
};

const Rectangle = ({rectangleItem}: RectangleType) => {

    const {id, color, width, height, x, y, rotation} = rectangleItem;
    const dimensionsBorder: DimensionsBorder = calculateDimensionsBorder();

    function calculateDimensionsBorder() {
        let widthBorder;
        let heightBorder;
        let rotationRadiusHW_WH = 0;
        let rotationRadiusHH_WW = 0;

        if (0 < rotation && rotation <= 90) {
            rotationRadiusHW_WH = rotation;
            rotationRadiusHH_WW = 90 - rotation;

        } else if (90 < rotation && rotation <= 180) {
            rotationRadiusHW_WH = 180 - rotation;
            rotationRadiusHH_WW = rotation - 90;
        }
        // todo-> + dwa else if'y dla (180 < rotation && rotation <= 270) i (270 < rotation && rotation <= 360)

        widthBorder = height * Math.sin((rotationRadiusHW_WH * Math.PI) / 180) +
            width * Math.sin((rotationRadiusHH_WW * Math.PI) / 180);
        heightBorder = width * Math.sin((rotationRadiusHW_WH * Math.PI) / 180) +
            height * Math.sin((rotationRadiusHH_WW * Math.PI) / 180);

        return {
            width: widthBorder,
            height: heightBorder
        }
    }

    return (
        <g>
            <rect
                id={id}
                fill={color.length === 7 ? color : "black"}
                width={width}
                height={height}
                x={`${x - width / 2}`}
                y={`${y - height / 2}`}
                transform={`rotate(${rotation} ${x} ${y})`}
            />
            <circle fill={"white"} r={4} cx={x} cy={y}/>
            <text
                fill={"white"}
                x={x + 7}
                y={y}>
                <tspan>{rotation}°</tspan>
            </text>
            <rect
                fill={"none"}
                stroke={"red"}
                strokeWidth={"1"}
                width={dimensionsBorder.width}
                height={dimensionsBorder.height}
                x={x - width / 2 - (dimensionsBorder.width - width) / 2}
                y={y - height / 2 - (dimensionsBorder.height - height) / 2}
            />
        </g>
    );
};
export default Rectangle;

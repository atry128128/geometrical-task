import React, {FormEvent} from "react";
import {getIdValue} from "../redux/selectors";
import {connect} from "react-redux";
import {setIdValue as setIdValueRedux} from "../redux/actions";
import {RootState} from "../types/StoreTypes";
import styled from "styled-components";

type PropsTypeIdValue = {
    setIdValueRedux: (a: string) => void;
}

const ButtonFetch = styled.button`
    cursor: crosshair;
    &:hover {
        transform: scale(1.4);
        background-color:yellow;
`;

const FormSearch = (props: PropsTypeIdValue) => {
    const [inputValue, setInputValue] = React.useState("");

    const handleSubmit = (event: FormEvent) => {
        event.preventDefault();
        props.setIdValueRedux(inputValue);
    };

    return (
        <form onSubmit={handleSubmit}>
            <input
                id="idValue"
                name="idValue"
                placeholder="type id project.."
                type="text"
                value={inputValue}
                onChange={e => setInputValue(e.target.value)}
            />
            <ButtonFetch type="submit">fetch</ButtonFetch>
        </form>
    );
};

const mapPropsToState = (state: RootState) => ({
    idValue: getIdValue(state)

});
export default connect(mapPropsToState, {setIdValueRedux})(FormSearch);

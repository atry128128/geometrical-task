import React from 'react';
import Rectangle from './Rectangle';
import {RequestRectanglesType} from "../types/RequestType";
import {RectangleType} from "../types/RectangleType";
import styled from "styled-components";

type RectangleListProps = {
    data?: RequestRectanglesType
}

const RectanglesWrapperBackground = styled.div`
    background-color: #C7C7C7;
    height: 85vh;
`;

const RectanglesList = ({data}: RectangleListProps) => {
    return (
        <>
            <RectanglesWrapperBackground>
                <svg version="1.1"
                     width="100%"
                     height="100%"
                     xmlns="http://www.w3.org/2000/svg"
                     preserveAspectRatio="xMidYMid meet">
                    <svg
                        version="1.1"
                        width="100%" height="100%"
                        viewBox={`0 0 ${data?.project?.width} ${data?.project?.height}`}
                        fill="#fff"
                        xmlns="http://www.w3.org/2000/svg">
                        <rect fill="#F7F7F7" width="100%" height="100%"/>
                        {data?.project && data?.project.items.map((item, key: number) =>
                            <Rectangle
                                key={key}
                                rectangleItem={item as RectangleType}
                            />)
                        }
                    </svg>
                </svg>
            </RectanglesWrapperBackground>
        </>
    )
}
export default RectanglesList;
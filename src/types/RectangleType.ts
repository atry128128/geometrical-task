export type RectangleType = {
    id: string,
    color: string,
    rotation: number,
    x: number,
    y: number,
    width: number,
    height: number
}
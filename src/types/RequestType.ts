export type RequestRectanglesType = {
    id: string,
    project: {
        id: string,
        name: string,
        width: number,
        height: number,
        items: {}[]
    }
}
import {RootState} from "../types/StoreTypes";

export const getIdValue = (store:RootState) => store.mainReducer?.idValue;

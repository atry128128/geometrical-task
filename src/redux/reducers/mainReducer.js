import {SET_IDVALUE} from "../actionTypes";

const initialState = {
    idValue: "",
};

export default function (state = initialState, action) {
    switch (action.type) {
        case SET_IDVALUE: {
            return {...state, idValue: action.payload.idValue};
        }
        default:
            return state;
    }
}
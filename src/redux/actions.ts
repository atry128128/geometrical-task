import { SET_IDVALUE } from "./actionTypes";


export const setIdValue = (idValue:string) => ({
    type: SET_IDVALUE,
    payload: {
        idValue
    }
});


